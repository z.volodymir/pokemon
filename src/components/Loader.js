import React from 'react'
import { Spinner } from 'react-bootstrap'

const Loader = () => (
  <Spinner animation="grow" role="status" variant="primary">
    <span className="visually-hidden">Loading...</span>
  </Spinner>
)

export default Loader
