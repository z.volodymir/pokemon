import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { Button, ListGroup, ListGroupItem, Modal } from 'react-bootstrap'

const DetailsModal = ({ showModal, setShowModal }) => {
  const [moves, setMoves] = useState([])
  const { pokemon } = useSelector((state) => state?.pokemon)

  useEffect(() => {
    if (pokemon.moves) {
      const movesList = pokemon.moves.map((item) => item.move.name)
      setMoves(movesList)
    }
  }, [pokemon.moves])

  const handleClose = () => setShowModal(false)
  return (
    <Modal show={showModal} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Moves List</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ListGroup style={{ maxHeight: '400px', overflow: 'scroll' }}>
          {moves &&
            moves.map((move) => (
              <ListGroupItem key={move} className="flex-fill">
                {move}
              </ListGroupItem>
            ))}
        </ListGroup>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

export default DetailsModal
