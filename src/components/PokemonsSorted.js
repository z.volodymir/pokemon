import React from 'react'
import { Form } from 'react-bootstrap'

const PokemonsSorted = ({ isSort, setIsSort }) => (
  <Form.Check
    type="switch"
    id="sort"
    label="Sort by name"
    className="d-flex justify-content-center align-items-center gap-2 mt-3"
    onClick={() => setIsSort(!isSort)}
  />
)

export default PokemonsSorted
