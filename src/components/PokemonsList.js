import React, { useEffect, useRef, useState } from 'react'
import { ListGroup } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { updatePokemonsAction } from '../redux/actions'
import useObserver from '../hooks/useObserver'
import PokemonsListItem from './PokemonsListItem'
import Loader from './Loader'

const PokemonsList = ({ filtered, isSort }) => {
  const [pokemons, setPokemons] = useState([])
  const [loading, setLoading] = useState(false)

  const { results, next } = useSelector((state) => state?.pokemons.pokemons)
  const { isLoading } = useSelector((state) => state?.pokemons)
  const dispatch = useDispatch()

  const lastElementRef = useRef(null)

  useEffect(() => {
    if (!filtered) {
      setLoading((state) => !state)
    }
  }, [filtered])

  useEffect(() => {
    if (results && isSort) {
      const sortedPokemons = [...results].sort((first, second) =>
        first.name.toLowerCase().localeCompare(second.name.toLowerCase())
      )
      setPokemons(sortedPokemons)
    } else if (results && !isSort) {
      setPokemons(results)
    }
    setLoading((state) => !state)
  }, [results, isSort])

  useObserver(loading, lastElementRef, next, () => {
    dispatch(updatePokemonsAction(next))
  })

  return (
    <>
      {isLoading && <Loader />}

      <ListGroup>
        {filtered
          ? filtered.map(({ name, url }, idx) => (
              <PokemonsListItem key={name} name={name} url={url} idx={idx} />
            ))
          : pokemons.map(({ name, url }, idx) => (
              <PokemonsListItem key={name} name={name} url={url} idx={idx} />
            ))}
        {!filtered && (
          <span ref={lastElementRef} className="overflow-hidden opacity-0" />
        )}
      </ListGroup>
    </>
  )
}

export default PokemonsList
