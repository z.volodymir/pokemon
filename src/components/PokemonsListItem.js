import React from 'react'
import { Button, ListGroup } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'

const PokemonsListItem = ({ name, url, idx }) => {
  const navigate = useNavigate()

  return (
    <ListGroup.Item
      style={{ overflowAnchor: 'none' }}
      className="d-grid gap-3 fw-bold"
      onClick={() => navigate(`pokemon/${name}`, { state: url })}
    >
      {idx + 1}. {name}
      <Button className="btn btn-primary text-light fw-bold">Details</Button>
    </ListGroup.Item>
  )
}

export default PokemonsListItem
