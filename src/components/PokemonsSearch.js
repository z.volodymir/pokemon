import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { FormControl, InputGroup } from 'react-bootstrap'
import { getAllPokemonsAction } from '../redux/actions'

const PokemonsSearch = ({ input, setInput, setFiltered }) => {
  const pokemons = useSelector((state) => state?.pokemonsAll)
  const dispatch = useDispatch()

  useEffect(() => {
    if (input.trim()) {
      const filteredPokemons = [...pokemons].filter((pokemon) =>
        pokemon.name.includes(input)
      )
      setFiltered(filteredPokemons)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [input])

  const onHandleFocus = () => {
    if (pokemons.length) return
    dispatch(getAllPokemonsAction())
  }

  const onHandleChange = (event) => {
    setInput(event.target.value)
  }

  return (
    <InputGroup>
      <InputGroup.Text id="search">Search</InputGroup.Text>
      <FormControl
        aria-label="Search"
        aria-describedby="search"
        value={input}
        onFocus={onHandleFocus}
        onChange={onHandleChange}
      />
    </InputGroup>
  )
}

export default PokemonsSearch
