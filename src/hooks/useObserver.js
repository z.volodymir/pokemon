import { useEffect, useRef } from 'react'

const useObserver = (loading, ref, nextPage, callback) => {
  const observerRef = useRef(null)

  useEffect(() => {
    if (observerRef.current) observerRef.current.disconnect()
    const observerCallback = (entries) => {
      if (entries[0].isIntersecting) {
        if (nextPage) {
          callback()
        } else {
          observerRef.current.disconnect()
        }
      }
    }
    observerRef.current = new IntersectionObserver(observerCallback)
    observerRef.current.observe(ref.current)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading])
}

export default useObserver
