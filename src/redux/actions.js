import { BASE_URL, fetchAPI } from '../api'
import {
  GET_ALL_POKEMONS,
  GET_POKEMON,
  GET_POKEMONS,
  SET_LOADING_POKEMON,
  SET_LOADING_POKEMONS,
  UPDATE_POKEMONS,
} from './types'

const setLoadingPokemonsAction = () => ({ type: SET_LOADING_POKEMONS })
const setLoadingPokemonAction = () => ({ type: SET_LOADING_POKEMON })

export const getAllPokemonsAction =
  (url = `${BASE_URL}pokemon?limit=100000&offset=0`) =>
  (dispatch) => {
    fetchAPI(url).then(({ data }) => {
      dispatch({
        type: GET_ALL_POKEMONS,
        payload: data.results,
      })
    })
  }

export const getPokemonsAction = () => {
  return (dispatch) => {
    dispatch(setLoadingPokemonsAction())

    fetchAPI(`${BASE_URL}pokemon?offset=0&limit=20`).then(({ data }) => {
      dispatch({
        type: GET_POKEMONS,
        payload: data,
      })
    })

    dispatch(setLoadingPokemonsAction())
  }
}

export const updatePokemonsAction = (path) => {
  return (dispatch) => {
    dispatch(setLoadingPokemonsAction())

    fetchAPI(path).then(({ data }) => {
      dispatch({
        type: UPDATE_POKEMONS,
        payload: data,
      })
    })

    dispatch(setLoadingPokemonsAction())
  }
}

export const getPokemonAction = (url) => (dispatch) => {
  dispatch(setLoadingPokemonAction())

  fetchAPI(url).then(({ data }) => {
    dispatch({
      type: GET_POKEMON,
      payload: data,
    })
  })

  dispatch(setLoadingPokemonAction())
}
