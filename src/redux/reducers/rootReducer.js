import { combineReducers } from 'redux'
import { pokemonsAllReducer } from './pokemonsAllReducer'
import { pokemonReducer } from './pokemonReduser'
import { pokemonsReducer } from './pokemonsReducer'

export const rootReducer = combineReducers({
  pokemonsAll: pokemonsAllReducer,
  pokemons: pokemonsReducer,
  pokemon: pokemonReducer,
})
