import { GET_POKEMONS, SET_LOADING_POKEMONS, UPDATE_POKEMONS } from '../types'

const initialState = {
  pokemons: {},
  isLoading: false,
}

export const pokemonsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_POKEMONS:
      return {
        ...state,
        pokemons: action.payload,
      }
    case UPDATE_POKEMONS:
      const { results, ...other } = action.payload
      return {
        ...state,
        pokemons: {
          ...other,
          results: [...state.pokemons.results, ...results],
        },
      }
    case SET_LOADING_POKEMONS:
      return { ...state, isLoading: !state.isLoading }
    default:
      return state
  }
}
