import { GET_POKEMON, SET_LOADING_POKEMON } from '../types'

const initialState = {
  pokemon: {},
  isLoading: false,
}

export const pokemonReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_POKEMON:
      return { ...state, pokemon: action.payload }
    case SET_LOADING_POKEMON:
      return { ...state, isLoading: !state.isLoading }
    default:
      return state
  }
}
