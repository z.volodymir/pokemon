import { GET_ALL_POKEMONS } from '../types'

const initialState = []

export const pokemonsAllReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_POKEMONS:
      return action.payload
    default:
      return state
  }
}
