import axios from 'axios'

export const BASE_URL = 'https://pokeapi.co/api/v2/'

export const fetchAPI = (path = '') => axios.get(path)
