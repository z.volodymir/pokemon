import React from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import HomePage from './pages/HomePage'
import DetailsPage from './pages/DetailsPage'

const App = () => (
  <Routes>
    <Route path="/" element={<HomePage />} />
    <Route path="pokemon/:name" element={<DetailsPage />} />
    <Route path="*" element={<Navigate to="/" />} />
  </Routes>
)

export default App
