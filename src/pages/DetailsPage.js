import React, { useEffect, useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { Button, ButtonGroup, Card, Col, Container, Row } from 'react-bootstrap'
import { getPokemonAction } from '../redux/actions'
import DetailsModal from '../components/DetailsModal'
import Loader from '../components/Loader'

const DetailsPage = () => {
  const [showModal, setShowModal] = useState(false)
  const { pokemon, isLoading } = useSelector((state) => state?.pokemon)
  const navigate = useNavigate()
  const location = useLocation()
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getPokemonAction(location.state))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const handleShow = () => setShowModal(true)

  return (
    <>
      {pokemon && (
        <Container>
          <Row style={{ minHeight: '100vh' }}>
            <Col
              sm={8}
              md={6}
              lg={4}
              className="d-flex justify-content-center align-items-center mx-auto mt-5"
            >
              {isLoading ? (
                <Loader />
              ) : (
                <Card>
                  <Card.Img
                    variant="top"
                    src={pokemon.sprites?.front_default}
                  />

                  <Card.Body>
                    <Card.Title>
                      <strong>Name: </strong>
                      {pokemon.name}
                    </Card.Title>

                    <DetailsModal
                      showModal={showModal}
                      setShowModal={setShowModal}
                    />

                    <ButtonGroup>
                      <Button
                        className="btn btn-secondary text-light"
                        onClick={() => navigate(-1)}
                      >
                        Back
                      </Button>
                      <Button variant="primary" onClick={handleShow}>
                        Show moves
                      </Button>
                    </ButtonGroup>
                  </Card.Body>
                </Card>
              )}
            </Col>
          </Row>
        </Container>
      )}
    </>
  )
}

export default DetailsPage
