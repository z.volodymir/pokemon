import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Col, Container, Form, Image, Row } from 'react-bootstrap'
import { getPokemonsAction } from '../redux/actions'
import PokemonsList from '../components/PokemonsList'
import PokemonsSearch from '../components/PokemonsSearch'
import PokemonsSorted from '../components/PokemonsSorted'

const HomePage = () => {
  const [filtered, setFiltered] = useState([])
  const [isSort, setIsSort] = useState(false)
  const [input, setInput] = useState('')
  const { results } = useSelector((state) => state?.pokemons.pokemons)
  const dispatch = useDispatch()

  useEffect(() => {
    if (!results) {
      dispatch(getPokemonsAction())
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <Container>
      <Row>
        <Col xs={12} sm={6} md={6} lg={4} className="mx-auto mt-5">
          <h1>
            <Image
              fluid
              src="https://raw.githubusercontent.com/PokeAPI/media/master/logo/pokeapi_256.png"
              className="d-block mx-auto"
              alt="PokeApi"
              aria-label="PokeApi"
            />
          </h1>
        </Col>
      </Row>
      <Row>
        <Col xs={12} sm={6} md={6} lg={4} className="mx-auto mt-5">
          <Form>
            <PokemonsSearch
              input={input}
              setInput={setInput}
              setFiltered={setFiltered}
            />
            <PokemonsSorted isSort={isSort} setIsSort={setIsSort} />
          </Form>
        </Col>
      </Row>
      <Row>
        <Col xs={12} sm={6} md={6} lg={4} className="text-center mx-auto mt-5">
          {input ? (
            <PokemonsList filtered={filtered} isSort={isSort} />
          ) : (
            <PokemonsList isSort={isSort} />
          )}
        </Col>
      </Row>
    </Container>
  )
}

export default HomePage
